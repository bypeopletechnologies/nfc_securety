package com.bypt4.supervisornfc;

import android.app.AlertDialog;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.bypt4.supervisornfc.Utils.CustomDialog;

import java.util.List;

/**
 * Created by bypt4 on 1/6/17.
 */

public class BaseFragment extends Fragment implements View.OnClickListener{

    protected Context mContext;
    protected FragmentManager fragmentManager;
    protected FragmentTransaction fragmentTransaction;

    protected ProgressDialog progressDialog;
    protected Handler handler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

    }
    private void init() {
        mContext = getContext();
        progressDialog = CustomDialog.ctor(mContext);
        fragmentManager = getActivity().getSupportFragmentManager();
        handler = new Handler();
    }

    protected void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    protected void setTypeFaceTextView(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }

    protected void setTypeFaceEdittext(EditText editText, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        editText.setTypeface(tf);
    }

    protected void setTypeFaceButton(Button button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    protected void setTypeFaceRadioButton(RadioButton button, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        button.setTypeface(tf);
    }

    protected void setTypeFaceCheckbox(CheckBox checkBox, String typeFace) {
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                typeFace);
        checkBox.setTypeface(tf);
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }
    protected void showDialogAlertPositiveButton(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton(mContext.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onClick(View view) {

    }
}
