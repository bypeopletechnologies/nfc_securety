package com.bypt4.supervisornfc.Utils;

/**
 * Created by bypt4 on 1/6/17.
 */

public class Constants {

    public static String IS_LOGGED_IN ="logged_in";
    public static boolean DEV_MODE = true;
    public static final int REQUEST_FOR_LOGIN = 1;
    public static final int REQUEST_FOR_GETBUILDINGLIST = 2;
    public static final int REQUEST_FOR_GETFLOORLIST = 3;
    public static final int REQUEST_FOR_GETNFCCODELIST = 4;
    public static final int REQUEST_FOR_ADDLOCATION = 5;
    public static final int SERVER_ERROR = 500;
    public static final String RESPONSE_OK = "1";
    public static final String EMPLOYEE_ID = "employee_id";
    public static final String EMPLOYEE_NAME = "employee_name";
    public static final String EMPLOYEE_PHONENUMBER = "employee_phone";
    public static final String EMPLOYEE_NO = "employee_no";

    public static final String EMPLOYEE_STATUS = "employee_status";
    public static final String EMPLOYEE_TYPE = "employee_type";
    public static final String EMPLOYEE_SESSIONCODE = "employee_sessioncode";

}
