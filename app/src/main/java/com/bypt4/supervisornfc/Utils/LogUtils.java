package com.bypt4.supervisornfc.Utils;

import android.util.Log;



/**
 * LogUtils Class for Handel Logger
 */
public class LogUtils {

    private static final String LOG_PREFIX = "Kinectem App";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOF_TAG_LENGTH = 23;


    private static boolean LOGGING_ENABLED = Constants.DEV_MODE;

    /**
     * Default Constructor
     */

    private LogUtils() {

    }

    /**
     * Method for create log for tag
     */

    private static String makeLogTag(String str) {
        if (str.length() > MAX_LOF_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOF_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }
        return LOG_PREFIX + str;
    }

    /**
     * Method for create log for tag
     */

    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    /**
     * Method for the print Debug Log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     */

    public static void LOG_D(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.DEBUG)) {
                Log.d(tag, message);
            }
        }
    }

    /**
     * Method for the print Debug Log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     * @param cause   : Error object
     */

    public static void LOG_D(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.DEBUG)) {
                Log.d(tag, message, cause);
            }
        }
    }

    /**
     * Method for print the verbose log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     */


    public static void LOG_V(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.VERBOSE)) {
                Log.v(tag, message);
            }
        }
    }

    /**
     * Method for print the verbose log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     * @param cause   : Error object
     */

    public static void LOG_V(final String tag, String message, Throwable cause) {

        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.VERBOSE)) {
                Log.v(tag, message, cause);
            }
        }
    }

    /**
     * method for print the information log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     */

    public static void LOG_I(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.INFO)) {
                Log.i(tag, message);
            }
        }
    }

    /**
     * Method for print the Information log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     * @param cause   : Error object
     */
    public static void LOG_I(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.INFO)) {
                Log.i(tag, message, cause);
            }
        }
    }

    /**
     * Method for print the Error Log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     */

    public static void LOG_E(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.ERROR)) {
                Log.e(tag, message);
            }
        }
    }

    /**
     * Method for print the Error Log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     * @param cause   : Error object
     */
    public static void LOGE(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.ERROR)) {
                Log.e(tag, message, cause);
            }
        }
    }
    /*
    *  Method for the Print warning Log
     *
     * @param tag it set tag for LOG
     * @param message set a message in log
     */

    public static void LOG_W(final String tag, String message) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.WARN)) {
                Log.w(tag, message);
            }
        }
    }

    /**
     * method for the print warning log
     *
     * @param tag     it set tag for LOG
     * @param message set a message in log
     * @param cause   : Error object
     */
    public static void LOG_W(final String tag, String message, Throwable cause) {
        if (LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.WARN)) {
                Log.w(tag, message, cause);
            }
        }
    }

}
