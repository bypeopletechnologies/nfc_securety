package com.bypt4.supervisornfc.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.bypt4.supervisornfc.R;


/**
 * Created by bypt4 on 4/10/16.
 */
public class CustomDialog extends ProgressDialog {
    private AnimationDrawable animation;

    public CustomDialog(Context context) {
        super(context);
    }

    private CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public static ProgressDialog ctor(Context context) {
        CustomDialog dialog = new CustomDialog(context, R.style.MyTheme);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cutome_progress_dialog);

        ImageView la = (ImageView) findViewById(R.id.animation);
        la.setBackgroundResource(R.drawable.custome_progress_dialog_animation);
        animation = (AnimationDrawable) la.getBackground();
    }

    @Override
    public void show() {
        super.show();
        if (animation != null)
            animation.start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (animation != null)
            animation.stop();
    }


}
