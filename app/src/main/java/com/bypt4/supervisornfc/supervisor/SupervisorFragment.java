package com.bypt4.supervisornfc.supervisor;

import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.ListPopupWindow;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.BaseFragment;
import com.bypt4.supervisornfc.NFCCallback.NfcCallback;
import com.bypt4.supervisornfc.R;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.Utils.LogUtils;
import com.bypt4.supervisornfc.Utils.NetworkUtil;
import com.bypt4.supervisornfc.http.HttpCallback;
import com.bypt4.supervisornfc.http.HttpPostRequest;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ABC on 6/2/2017.
 */



    public class SupervisorFragment extends BaseFragment implements HttpCallback,NfcCallback {

    private static final String TAG = "SupervisorFragment";
    private View rootView;
    private EditText sp_selectbuilding,sp_selectfloor,sp_selectnfctag;
    private ImageView imageView_writetag;
    ArrayList<String> buildingnamelist,buildingidlist,flooridlist,floornamelist,locationno,locationid,locationname;
    private String selectedbuilding,selectedfloor,selectedFloorName,selectedlocationname,selectedlocationid,selectedbuildingname;
    private Tag tag;
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            mContext =context;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            if (rootView == null)
            {
                rootView= inflater.inflate(R.layout.layout_supervisor,container,false);
                init();
                setListeners();
                setFontStyle();
                getBuildingList();
                return rootView;
            }
            else {
                return rootView;
            }
        }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if(intent!=null) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            if(tag!=null) {
                LogUtils.LOG_E(TAG, "onNewIntent: " + intent.getAction());
                Ndef ndef = Ndef.get(tag);
                ((BaseActivity) mContext).readFromNFC(ndef);
            }
        }
    }

    private void init() {
            sp_selectbuilding = (EditText) rootView.findViewById(R.id.etSelectBuilding);
            sp_selectfloor = (EditText) rootView.findViewById(R.id.etSelectFloor);
            sp_selectnfctag = (EditText) rootView.findViewById(R.id.etSelectFootPrint);
            imageView_writetag=(ImageView)rootView.findViewById(R.id.ivTagConfirm);
            ((BaseActivity)mContext).showToolbar(true);
            ((BaseActivity)mContext).setToolbarTitle(SharedPreferenceUtil.
                getString(Constants.EMPLOYEE_NAME,getString(R.string.supervisor_name)));
            ((BaseActivity)mContext).showHideToolbarBackButton(false);
            ((BaseActivity)mContext).setNFCResponseCallBack(this);
            ((BaseActivity)mContext).showHideToolbarRightButton(true);

            buildingidlist = new ArrayList<>();
            buildingnamelist = new ArrayList<>();
            flooridlist = new ArrayList<>();
            floornamelist = new ArrayList<>();
            locationid = new ArrayList<>();
            locationno = new ArrayList<>();
            locationname = new ArrayList<>();

        }
        private void setListeners() {
            imageView_writetag.setOnClickListener(this);
            sp_selectbuilding.setOnClickListener(this);
            sp_selectfloor.setOnClickListener(this);
            sp_selectnfctag.setOnClickListener(this);
        }
        private void setFontStyle() {
            //setTypeFaceTextView();
        }

    private void getBuildingList()
    {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("employee_id", SharedPreferenceUtil.getString(Constants.EMPLOYEE_ID, ""));
            params.put("sessionCode", SharedPreferenceUtil.getString(Constants.EMPLOYEE_SESSIONCODE, ""));
            HttpPostRequest request = new HttpPostRequest(mContext.getString(R.string.base_url)
                        + "getBuildingList", params, Constants.REQUEST_FOR_GETBUILDINGLIST, this);
                new Thread(request).start();
            } else {
                showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_check_network));
            }
    }

    private void getFloorList()
    {

        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("employee_id", SharedPreferenceUtil.getString(Constants.EMPLOYEE_ID, ""));
            params.put("sessionCode", SharedPreferenceUtil.getString(Constants.EMPLOYEE_SESSIONCODE, ""));
            params.put("building_id", selectedbuilding);

            HttpPostRequest request = new HttpPostRequest(mContext.getString(R.string.base_url)
                    + "getFloorListByBuildingId", params, Constants.REQUEST_FOR_GETFLOORLIST, this);
            new Thread(request).start();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_check_network));
        }

    }

    private void getNFCCodeList()
    {

        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("employee_id", SharedPreferenceUtil.getString(Constants.EMPLOYEE_ID, ""));
            params.put("sessionCode", SharedPreferenceUtil.getString(Constants.EMPLOYEE_SESSIONCODE, ""));
            params.put("floor_id", selectedfloor);

            HttpPostRequest request = new HttpPostRequest(mContext.getString(R.string.base_url)
                    + "getLocationListByFloorId", params, Constants.REQUEST_FOR_GETNFCCODELIST, this);
            new Thread(request).start();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_check_network));
        }

    }

    private void showBuildingList() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, buildingnamelist);

        final ListPopupWindow lpw = new ListPopupWindow(mContext);
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(sp_selectbuilding);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = buildingnamelist.get(position);
                sp_selectbuilding.setText(item);

                selectedbuilding=buildingidlist.get(position);
                selectedbuildingname=buildingnamelist.get(position);
                LogUtils.LOG_E("Selecting Building:",selectedbuilding);

                flooridlist.clear();
                floornamelist.clear();
                sp_selectfloor.setHint(mContext.getString(R.string.select_floor));
                sp_selectfloor.setText(mContext.getString(R.string.select_floor));


                sp_selectnfctag.setHint(mContext.getString(R.string.select_nfc_tagpoint));
                sp_selectnfctag.setText(mContext.getString(R.string.select_nfc_tagpoint));
                getFloorList();
                lpw.dismiss();
            }
        });
        lpw.show();
    }
    private void showFloorList() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, floornamelist);

        final ListPopupWindow lpw = new ListPopupWindow(mContext);
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(sp_selectfloor);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = floornamelist.get(position);
                sp_selectfloor.setText(item);
                selectedfloor = flooridlist.get(position);
                selectedFloorName = floornamelist.get(position);
                LogUtils.LOG_E(TAG,"FoorId"+selectedfloor);
                locationno.clear();
                locationid.clear();
                locationname.clear();
                sp_selectnfctag.setHint(mContext.getString(R.string.select_nfc_tagpoint));
                sp_selectnfctag.setText(mContext.getString(R.string.select_nfc_tagpoint));
                getNFCCodeList();
                lpw.dismiss();
            }
        });
        lpw.show();
    }
    private void showLocationList() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_list_item_1, locationno);

        final ListPopupWindow lpw = new ListPopupWindow(mContext);
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(sp_selectnfctag);
        lpw.setModal(true);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = locationno.get(position);
                sp_selectnfctag.setText(item);
                selectedlocationname = locationname.get(position);
                selectedlocationid = locationid.get(position);
                lpw.dismiss();
            }
        });
        lpw.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.etSelectBuilding :
                showBuildingList();
                break;
            case R.id.etSelectFloor :
                showFloorList();
                break;
            case R.id.etSelectFootPrint:
                showLocationList();
                break;
            case R.id.ivTagConfirm:
                if(isValidDetails()){
                    ((BaseActivity)mContext).writeNdefMessage(selectedlocationid+","+selectedlocationname
                            +"-"+selectedFloorName+"-"+selectedbuildingname);
                    LogUtils.LOG_E("WriteData:",selectedlocationid+","+selectedlocationid
                            +" "+selectedFloorName+" "+selectedbuildingname);
                }
        }
    }


    private boolean isValidDetails(){
        if(sp_selectbuilding.getText().toString().trim().length() < 1
                || sp_selectbuilding.getText().toString().equalsIgnoreCase(getString(R.string.select_building)) ){
            showDialogAlertPositiveButton(getString(R.string.app_name),getString(R.string.alert_select_building));

            return false;
        }
        if(sp_selectfloor.getText().toString().trim().length() < 1
                || sp_selectfloor.getText().toString().equalsIgnoreCase(getString(R.string.select_floor))){
            showDialogAlertPositiveButton(getString(R.string.app_name),getString(R.string.alert_select_floor));

            return false;
        }
        if(sp_selectnfctag.getText().toString().trim().length() < 1
                || sp_selectnfctag.getText().toString().equalsIgnoreCase(getString(R.string.select_nfc_tagpoint))){
            showDialogAlertPositiveButton(getString(R.string.app_name),getString(R.string.alert_select_tag));

            return false;
        }
        return true;
    }
    @Override
    public void onResponse(final String response, int action) {
        progressDialog.dismiss();
        if (HttpPostRequest.statusCode != Constants.SERVER_ERROR) {
            if (response != null) {
                if (action == Constants.REQUEST_FOR_GETBUILDINGLIST) {
                    Log.e("Building List Res:", response);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    JSONArray buildingarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < buildingarray.length(); i++) {
                                        JSONObject jsonObject1=buildingarray.getJSONObject(i);
                                        LogUtils.LOG_E("Building Id:",jsonObject1.getString("building_id"));
                                        LogUtils.LOG_E("Building Id:",jsonObject1.getString("building_name"));
                                       // LogUtils.LOG_E("Location Name",jsonObject1.getString());
                                        buildingidlist.add(jsonObject1.getString("building_id"));
                                        buildingnamelist.add(jsonObject1.getString("building_name"));
                                    }
                                }else {
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), jsonObject.getString("message"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (action == Constants.REQUEST_FOR_GETFLOORLIST) {
                    Log.e("Floor List:", response);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    JSONArray buildingarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < buildingarray.length(); i++) {
                                        JSONObject jsonObject1=buildingarray.getJSONObject(i);
                                        LogUtils.LOG_E("Floor Id:",jsonObject1.getString("floor_id"));
                                        LogUtils.LOG_E("Floor Id:",jsonObject1.getString("floor_name"));
                                        flooridlist.add(jsonObject1.getString("floor_id"));
                                        floornamelist.add(jsonObject1.getString("floor_name"));
                                    }
                                }else {
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), jsonObject.getString("message"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (action == Constants.REQUEST_FOR_GETNFCCODELIST) {
                    Log.e("NFC CARD List:", response);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    JSONArray buildingarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < buildingarray.length(); i++) {
                                        JSONObject jsonObject1=buildingarray.getJSONObject(i);
                                        LogUtils.LOG_E("location no",jsonObject1.getString("location_no"));
                                        LogUtils.LOG_E("location id:",jsonObject1.getString("location_id"));
                                        LogUtils.LOG_E("location name:",jsonObject1.getString("location_name"));
                                        locationid.add(jsonObject1.getString("location_id"));
                                        locationno.add(jsonObject1.getString("location_no"));
                                        locationname.add(jsonObject1.getString("location_name"));

                                    }
                                }else {
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), jsonObject.getString("message"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                    }
                });
            }
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                }
            });
        }
    }
}


