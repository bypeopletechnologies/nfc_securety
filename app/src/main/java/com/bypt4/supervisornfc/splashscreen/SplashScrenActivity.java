package com.bypt4.supervisornfc.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.R;

/**
 * Created by bypt4 on 1/6/17.
 */

public class SplashScrenActivity extends Activity {
    private static final int SPLASH_DISPLAY_TIME = 2000; // splash screen delay time

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash_screen);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
       init();
    }

    /**
     * Initialize View & activity component
     */
    private void init() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent();
                intent.setClass(SplashScrenActivity.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY_TIME);
    }
}
