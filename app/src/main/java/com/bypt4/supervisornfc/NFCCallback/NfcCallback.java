package com.bypt4.supervisornfc.NFCCallback;

/**
 * 
 * @author Sandeep
 *
 */
public interface NfcCallback {
   
	/**
	 * Callback method for NFC response. called when an NFC Tag is received.
	 * @param response
	 * @param action
	 */
    public void onResponse(String response, int action);
}