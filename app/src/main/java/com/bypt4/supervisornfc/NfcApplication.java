package com.bypt4.supervisornfc;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by bypt4 on 1/6/17.
 */

public class NfcApplication extends MultiDexApplication {
    private Context mContext;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mContext = this;
        SharedPreferenceUtil.init(mContext);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
