package com.bypt4.supervisornfc;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;

import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bypt4.supervisornfc.NFCCallback.NfcCallback;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.guard.GuardNameFragment;
import com.bypt4.supervisornfc.login.LoginFragment;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;
import com.bypt4.supervisornfc.supervisor.SupervisorFragment;

import java.io.IOException;
import java.nio.charset.Charset;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "BaseActivity";
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private NfcAdapter nfcAdapter;
    private Tag tag;
    private LinearLayout toolbar;
    private TextView txtViewToolbarTitle;
    private ImageView ivToolbarbackicon,ivToolbarRightIcon;
    private String tagMessage;
    private NfcCallback cb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        init();
        initNFC();
        setListeners();
        handleScreenNavigation();
    }

    private void init() {
        toolbar = (LinearLayout) findViewById(R.id.layoutToolbar);
        ivToolbarbackicon = (ImageView) findViewById(R.id.ivToolbarbackicon);
        txtViewToolbarTitle=(TextView)findViewById(R.id.tvToolbarTitle);
        ivToolbarbackicon=(ImageView)findViewById(R.id.ivToolbarbackicon);
        ivToolbarRightIcon= (ImageView) findViewById(R.id.ivToolbarRightIcon);
        setTypeFaceToolbarTitle(txtViewToolbarTitle,this.getString(R.string.fontLatoRegular));
        fragmentManager = getSupportFragmentManager();
    }

    private void initNFC() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (nfcAdapter != null){
            nfcAdapter.disableForegroundDispatch(this);
           }else{
            initNFC();
            if (nfcAdapter!=null)
                nfcAdapter.disableForegroundDispatch(this);
           }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (nfcAdapter != null)
        {
            enableNFCForegroundDispatch();
        }
        else
            initNFC();
            enableNFCForegroundDispatch();

    }

    private void enableNFCForegroundDispatch() {
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected, tagDetected, ndefDetected};

        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(), 0, new Intent(getApplicationContext(),
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if (nfcAdapter != null)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if(intent!=null) {
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            if(tag!=null) {
                setIntent(intent);
                if(cb != null){
                    cb.onResponse(readFromNFC(Ndef.get(tag)),1);
                }
            }
        }

    }

    public void setNFCResponseCallBack(NfcCallback cb){
        this.cb = cb;

    }

    public String readFromNFC(Ndef ndef) {
        try {
            if (ndef != null)
            {
                ndef.connect();
                NdefMessage ndefMessage = ndef.getNdefMessage();
                if (ndefMessage != null)
                {
                    tagMessage = new String(ndefMessage.getRecords()[0].getPayload());
                    Log.e(TAG, "readFromNFC: " + tagMessage);
                }
                ndef.close();
            }

        } catch (IOException | FormatException e) {
            e.printStackTrace();

        }
        return tagMessage;
    }


    public void writeNdefMessage(String ndefMessage){

        try {

            if(tag != null) {

                Ndef ndef = Ndef.get(tag);

                if (ndef == null) {

                    Toast.makeText(this, "NDEF is null", Toast.LENGTH_SHORT).show();
                    formatTeg(tag, ndefMessage);

                } else {

                    if (!ndef.isWritable()) {
                        Toast.makeText(this, "Tag is not writeable", Toast.LENGTH_SHORT).show();
                        ndef.close();
                        return;
                    }

                    NdefRecord appRecord = NdefRecord.createApplicationRecord("com.android.bypt4.supervisornfc");
                    ndef.connect();
                    NdefRecord mimeRecord = NdefRecord.createMime("text/plain", ndefMessage.getBytes(Charset.forName("US-ASCII")));
                    ndef.writeNdefMessage(new NdefMessage(mimeRecord, appRecord));
                    ndef.close();
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.locaion_tag_scuccess)
                            .setCancelable(false)
                            .setTitle(getString(R.string.app_name))
                            .setPositiveButton(this.getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            Fragment supervisorFragment = new SupervisorFragment();
                                            fragmentTransaction = fragmentManager.beginTransaction();
                                            fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                                    supervisorFragment);
                                            fragmentTransaction.commitNowAllowingStateLoss();
                                        }
                                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                }
            }
            else {
                showToast("Tag not found for write");
            }

        }catch (Exception e){
            Log.e(TAG,"write tag  "+e.getMessage());
        }
    }

    private void formatTeg(Tag tag, String message){
        try{
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            NdefRecord appRecord = NdefRecord.createApplicationRecord("com.android.bypt4.supervisornfc");
            if(ndefFormatable == null){
                Toast.makeText(this,"Tag is not Ndef formative",Toast.LENGTH_SHORT).show();
                return;
            }
            NdefRecord mimeRecord = NdefRecord.createMime("text/plain", message.getBytes(Charset.forName("US-ASCII")));
            NdefMessage ndefMessage = new NdefMessage(mimeRecord,appRecord);
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.locaion_tag_scuccess))
                    .setCancelable(false)
                    .setTitle(getString(R.string.app_name))
                    .setPositiveButton(this.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    Fragment supervisorFragment = new SupervisorFragment();
                                    fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                            supervisorFragment);
                                    fragmentTransaction.commitNowAllowingStateLoss();
                                }
                            });

            AlertDialog alert = builder.create();
            alert.show();

        }catch (Exception e){
            Log.e("FormetTag",e.getMessage());
        }

    }

    private void setListeners() {
        ivToolbarbackicon.setOnClickListener(this);
        ivToolbarRightIcon.setOnClickListener(this);
    }


    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void handleScreenNavigation()
    {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (SharedPreferenceUtil.getBoolean(Constants.IS_LOGGED_IN,false))
        {
            if(SharedPreferenceUtil.getString(Constants.EMPLOYEE_TYPE,"").equals("2"))
            {
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                            new SupervisorFragment()).commitAllowingStateLoss();
            }
            else
            {
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                            new GuardNameFragment()).commitAllowingStateLoss();
            }
        }
        else
        {
            fragmentTransaction.replace(R.id.frameLayoutBaseContainer,new LoginFragment()).commitAllowingStateLoss();
        }
    }

    public void showToolbar(boolean isShow)
    {
        if(isShow)
        {
            toolbar.setVisibility(View.VISIBLE);
        }else
        {
            toolbar.setVisibility(View.GONE);
        }
    }

    public void showHideToolbarBackButton(boolean isShow)
    {
        if(isShow)
        {
            ivToolbarbackicon.setVisibility(View.VISIBLE);
        }else
        {
            ivToolbarbackicon.setVisibility(View.GONE);
        }
    }
    public void showHideToolbarRightButton(boolean isShow)
    {
        if(isShow)
        {
            ivToolbarRightIcon.setVisibility(View.VISIBLE);
        }else
        {
            ivToolbarRightIcon.setVisibility(View.GONE);
        }
    }

    public void setToolbarTitle(String title) {
        txtViewToolbarTitle.setText(title);
    }

    public void setTypeFaceToolbarTitle(TextView txtView, String typeFace) {
        Typeface tf = Typeface.createFromAsset(this.getAssets(),
                typeFace);
        txtView.setTypeface(tf);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivToolbarbackicon:
            fragmentManager.popBackStack();
            break;
            case R.id.ivToolbarRightIcon:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.log_out))
                        .setCancelable(false)
                        .setTitle(getString(R.string.app_name))
                        .setPositiveButton(this.getString(R.string.yes),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        SharedPreferenceUtil.clear();
                                        Fragment supervisorFragment = new LoginFragment();
                                        fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer,
                                                supervisorFragment);
                                        fragmentTransaction.commit();
                                    }
                                })
                        .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                ;

                AlertDialog alert = builder.create();
                alert.show();
                break;
        }
    }
}
