package com.bypt4.supervisornfc.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.BaseFragment;
import com.bypt4.supervisornfc.R;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.Utils.LogUtils;
import com.bypt4.supervisornfc.Utils.NetworkUtil;
import com.bypt4.supervisornfc.guard.GuardNameFragment;
import com.bypt4.supervisornfc.http.HttpCallback;
import com.bypt4.supervisornfc.http.HttpPostRequest;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;
import com.bypt4.supervisornfc.supervisor.SupervisorFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by bypt4 on 1/6/17.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener, HttpCallback {

    private View rootView;
    private EditText editText_username, editText_password;
    private ImageView iv_login;
    private TextView tv_forgotpassword;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.layout_login, container, false);
            init();
            setListeners();
            setFontStyle();

            return rootView;
        } else {
            return rootView;
        }
    }

    private void init() {
        editText_username = (EditText) rootView.findViewById(R.id.et_username);
        editText_password = (EditText) rootView.findViewById(R.id.et_password);
        iv_login = (ImageView) rootView.findViewById(R.id.iv_login);
        ((BaseActivity)mContext).showToolbar(false);
    }

    private void setListeners() {
        iv_login.setOnClickListener(this);
    }

    private void setFontStyle() {
        //setTypeFaceTextView();
        setTypeFaceEdittext(editText_username, mContext.getString(R.string.fontLatoLight));
        setTypeFaceEdittext(editText_password, mContext.getString(R.string.fontLatoLight));

    }

    private boolean isValidDetails() {
        if (editText_username.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_enter_your_username_pass));
            editText_username.requestFocus();
            return false;
        }

        if (editText_password.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_enter_your_pass));
            editText_password.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.iv_login:
                if (isValidDetails()) {
                    hideKeyboard();
                    sendRequestForLogin();
                }
                break;
            default:
                break;
        }
    }

    private void sendRequestForLogin() {
        if (NetworkUtil.isOnline(mContext)) {
           progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("phone_number", editText_username.getText().toString().trim());
            params.put("device_token", "test_device");
            params.put("password", editText_password.getText().toString().trim());
            HttpPostRequest request = new HttpPostRequest(mContext.getString(R.string.base_url)
                    + "login", params, Constants.REQUEST_FOR_LOGIN, this);
            new Thread(request).start();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_check_network));
        }

    }


    @Override
    public void onResponse(final String response, int action) {
        progressDialog.dismiss();
        if (HttpPostRequest.statusCode != Constants.SERVER_ERROR) {
            if (response != null) {
                if (action == Constants.REQUEST_FOR_LOGIN) {
                    Log.e("Login Res:", response);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                LogUtils.LOG_E("Handler in","Handler");
                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                  JSONObject userObject = jsonObject.getJSONObject("data");
                                  JSONObject employee_data=userObject.getJSONObject("employee_data");
                                    JSONObject session_data=userObject.getJSONObject("session_data");
                                    LogUtils.LOG_E("Employee Data:",employee_data.toString());
                                    LogUtils.LOG_E("SESSION DATA:",session_data.toString());

                                    LogUtils.LOG_E("Employee ID:",String.valueOf(employee_data.getInt("employee_id")));
                                    LogUtils.LOG_E("Employee Name:",employee_data.getString("employee_name"));
                                    LogUtils.LOG_E("SESSION CODE:",session_data.getString("sessionCode"));


                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_ID, String.valueOf(employee_data.getInt("employee_id")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_NAME, String.valueOf(employee_data.getString("employee_name")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_PHONENUMBER, String.valueOf(employee_data.getString("phone_number")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_NO, String.valueOf(employee_data.getString("employee_no")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_STATUS, String.valueOf(employee_data.getString("status")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_TYPE, String.valueOf(employee_data.getString("employee_type")));
                                    SharedPreferenceUtil.putValue(Constants.EMPLOYEE_SESSIONCODE, String.valueOf(session_data.getString("sessionCode")));
                                    SharedPreferenceUtil.putValue(Constants.IS_LOGGED_IN, true);

                                    SharedPreferenceUtil.save();
                                    if(employee_data.getString("employee_type").equals("2"))
                                    {

                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new SupervisorFragment()).commitAllowingStateLoss();
                                    }
                                   else
                                    {
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                                        fragmentTransaction.replace(R.id.frameLayoutBaseContainer, new GuardNameFragment()).commitAllowingStateLoss();
                                    }
                                } else {
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), jsonObject.getString("message"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                    }
                });
            }
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                }
            });
        }
    }
}

