package com.bypt4.supervisornfc.http;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * HttpPost request class
 * 
 * 
 */

/**
 * @author Nayan
 *
 */
public class HttpPostRequest implements Runnable {

	private final static int TIMEOUT = 50000;
	private static final char PARAMETER_DELIMITER = '&';
	private static final char PARAMETER_EQUALS_CHAR = '=';
	public static int statusCode;
	private String strUrl;
	private int action;
	private HttpCallback cb;
	private HashMap<String, String> params = null;

	/**
	 * HttpPostRequest Runnable Object
	 * @param params: List of request params
	 * @param action: Type of call back Action
	 * @param cb: CallBack call object
	 */
	public HttpPostRequest(String strUrl, HashMap<String, String> params,
						   int action, HttpCallback cb) {

		this.strUrl = strUrl;
		this.cb = cb;
		this.action = action;
		this.params = params;

	}

	@Override
	public void run() 
	{
		HttpURLConnection urlConnection = null;
		ByteArrayOutputStream arrayOutputStream=null;
		InputStream inputStream = null;
		
		try
		{
			URL url = new URL(strUrl);
			Log.e("<<URL>>",strUrl);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setReadTimeout(TIMEOUT);
			urlConnection.setConnectTimeout(TIMEOUT);
			urlConnection.setRequestMethod("POST");
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			String paramsString = createQueryStringForParameters(params);
//			urlConnection.setFixedLengthStreamingMode(paramsString.getBytes().length);
			OutputStream os = urlConnection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(paramsString);
			writer.flush();
			writer.close();
			os.close();
			urlConnection.connect();
			
			// handle issues
	        statusCode = urlConnection.getResponseCode();
	        byte[] responseData = new byte[1000];
			int length = 0;
			arrayOutputStream = new ByteArrayOutputStream();
			
	        if (statusCode != HttpURLConnection.HTTP_OK)
	        {
	        	Log.e("statusCode", +statusCode+" :: "+urlConnection.getErrorStream());
				inputStream  = new BufferedInputStream(urlConnection.getErrorStream());
	        }
	        else
	        {
	        	inputStream  = new BufferedInputStream(urlConnection.getInputStream());
	        }
			
			while ((length = inputStream.read(responseData)) != -1) 
			{
				arrayOutputStream.write(responseData, 0, length);
			}
			  
			String rawResponse =	new String(arrayOutputStream.toByteArray());
			arrayOutputStream.close();

			cb.onResponse(rawResponse,action);
		
		} 
		catch (MalformedURLException e)
		{
			// handle invalid URL
			cb.onResponse(null,action);
		}
		catch (SocketTimeoutException e)
		{
			// hadle timeout
			cb.onResponse(null,action);
		} 
		catch (IOException e)
		{
			// handle I/0
			cb.onResponse(null,action);
		}
		catch (Exception e)
		{
			// handle I/0
			cb.onResponse(null,action);
			e.printStackTrace();
		}
		finally
		{
			close(inputStream);
			close(arrayOutputStream);
			if (urlConnection != null) 
			{
				urlConnection.disconnect();
			}
		}
	}
	public static String createQueryStringForParameters(Map<String, String> parameters)
	{
	    StringBuilder parametersAsQueryString = new StringBuilder();
	    if (parameters != null) 
	    {
	        boolean firstParameter = true;
	         
	        for (String parameterName : parameters.keySet())
	        {
	            if (!firstParameter)
	            {
	                parametersAsQueryString.append(PARAMETER_DELIMITER);
	            } 
	             
	            try 
	            {
					parametersAsQueryString.append(parameterName)
					    .append(PARAMETER_EQUALS_CHAR)
					    .append(URLEncoder.encode(parameters.get(parameterName),"UTF-8"));
				}
	            catch (UnsupportedEncodingException e)
	            {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	             
	            firstParameter = false;
	        }
	    }
	    Log.e("<<< URL >>>", parametersAsQueryString.toString());
	    return parametersAsQueryString.toString();
	}
	
	/**Closes this stream. This releases system resources used for this stream.
	 * @param arrayOutputStream
	 */
	private void close(ByteArrayOutputStream arrayOutputStream)
	{
		// TODO Auto-generated method stub
		try 
		{
			if (arrayOutputStream != null)
			{
				arrayOutputStream.close();
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**Closes this stream. This releases system resources used for this stream.
	 * @param
	 */
	private void close(InputStream inputStream)
	{
		try
		{
			if (inputStream != null)
			{
				inputStream.close();
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
