package com.bypt4.supervisornfc.http;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Http GET request class
 *
 * @author Sandeep_PC
 *
 */
public class HttpRequest implements Runnable {

	private final static int TIMEOUT = 120000;

	private String urlstring;
	private int action;
	public static int statusCode;
	private HttpCallback cb;

	private HashMap<String, String> params = null;

	/**
	 * HttpRequest Runnable Object
	 *
	 * @param url
	 *            : Base url
	 * @param params
	 *            : List of request params
	 * @param action
	 *            : Type of call back Action
	 * @param cb
	 *            : CallBack call object
	 */
	public HttpRequest(String url, HashMap<String, String> params, int action,
					   HttpCallback cb) {

		this.urlstring = url;
		this.cb = cb;
		this.action = action;
		this.params = params;
	}

	@SuppressWarnings("resource")
	public void run() 
	{

		String rawResponse = null;
		HttpURLConnection urlConnection = null;
		StringBuilder builder = new StringBuilder(urlstring);
		InputStream inputStream = null;

		try
		{

			if (params != null) 
			{

				builder.append("?");
				Set<String> set = params.keySet();

				for (Iterator<String> iterator = set.iterator(); iterator
						.hasNext();) 
				{

					String paramName = iterator.next();
					if (params.get(paramName) != null && paramName != null) 
					{
						builder.append(paramName.trim())
								.append("=")
								.append(URLEncoder.encode(params.get(paramName)
										.trim(), "UTF-8")).append("&");
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConcurrentModificationException e) {
			// TODO: handle exception
			e.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try 
		{
			Log.e("<<url>>", builder.toString());
			URL url = new URL(builder.toString());
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setConnectTimeout(TIMEOUT);
			urlConnection.setReadTimeout(TIMEOUT);

			byte[] responseData = new byte[1000];
			int length = 0;

			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			statusCode= urlConnection.getResponseCode();
			
			if (statusCode == HttpURLConnection.HTTP_OK) {
				inputStream  = new BufferedInputStream(urlConnection.getInputStream());
	         }

			while ((length = inputStream.read(responseData)) != -1) {
				arrayOutputStream.write(responseData, 0, length);
			}

			rawResponse = new String(arrayOutputStream.toByteArray());
			arrayOutputStream.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			close(inputStream);
			 urlConnection.disconnect();
		}

		if (rawResponse != null) {
			cb.onResponse(rawResponse.toString(), action);
		} else
			cb.onResponse(null, action);
	}

	public static void close(InputStream is) {
		try {
			if (is != null) {
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}