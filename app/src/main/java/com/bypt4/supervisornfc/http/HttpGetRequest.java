package com.bypt4.supervisornfc.http;


import com.bypt4.supervisornfc.Utils.LogUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpGetRequest implements Runnable {

    private final static int TIMEOUT = 50000;
    private static final char PARAMETER_DELIMITER = '&';
    private String strUrl;
    private int action;
    private HttpCallback cb;
    private HashMap<String, String> authenticationParam;
    private HashMap<String, String> params = null;
    private String rawResponse;
    public static int statusCode;

    public HttpGetRequest(String strUrl, HashMap<String, String> authenticationParam, int action, HttpCallback cb) {
        this.strUrl = strUrl;
        this.action = action;
        this.cb = cb;
        this.authenticationParam = authenticationParam;
    }

    public HttpGetRequest(String strUrl, HashMap<String, String> authenticationParam, HashMap<String, String> params, int action, HttpCallback cb) {
        this.strUrl = strUrl;
        this.action = action;
        this.cb = cb;
        this.authenticationParam = authenticationParam;
        this.params = params;
    }

    private static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;
            parametersAsQueryString.append("?");
            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }
                parametersAsQueryString.append(parameterName)
                        .append("=")
                        .append(parameters.get(parameterName));
                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }

    @Override
    public void run() {

        HttpURLConnection urlConnection = null;
        ByteArrayOutputStream arrayOutputStream = null;
        InputStream inputStream = null;
        try {
            URL url = new URL(strUrl + createQueryStringForParameters(params));
            LogUtils.LOG_I("<<< URL >>>", url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setReadTimeout(TIMEOUT);
            urlConnection.setConnectTimeout(TIMEOUT);
            urlConnection.setRequestMethod("GET");

            if (authenticationParam != null)
                urlConnection = createUrlConnectionRequest(urlConnection, authenticationParam);
            urlConnection.connect();

            // handle issues
            statusCode = urlConnection.getResponseCode();

            LogUtils.LOG_E("Status Code",statusCode+"");
            byte[] responseData = new byte[4000];
            int length;
            arrayOutputStream = new ByteArrayOutputStream();

            if (statusCode != HttpURLConnection.HTTP_OK) {
                LogUtils.LOG_D("statusCode", +statusCode + " :: " + urlConnection.getErrorStream());
                inputStream = new BufferedInputStream(urlConnection.getErrorStream());
            } else {
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
            }

            while ((length = inputStream.read(responseData)) != -1) {
                arrayOutputStream.write(responseData, 0, length);
            }
             rawResponse = new String(arrayOutputStream.toByteArray());

            arrayOutputStream.close();
            LogUtils.LOG_I("Http Get", rawResponse);



        } catch (ProtocolException | MalformedURLException | SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(arrayOutputStream);
            close(inputStream);

            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            cb.onResponse(rawResponse, action);
        }

    }

    private HttpURLConnection createUrlConnectionRequest(HttpURLConnection urlConnection, Map<String, String> requestmap) {

        if (requestmap != null) {
            for (String parameterName : requestmap.keySet()) {
                urlConnection.setRequestProperty(parameterName, requestmap.get(parameterName));
                LogUtils.LOG_I("Auth param in GEt", requestmap.get(parameterName) + "");
            }
        }
        return urlConnection;
    }

    /**
     * Closes this stream. This releases system resources used for this stream.
     *
     * @param inputStream Object of InputStream for release resource
     */

    private void close(InputStream inputStream) {
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes this stream. This releases system resources used for this stream.
     *
     * @param arrayOutputStream Object of ByArrayOutPutStream for release resource
     */
    private void close(ByteArrayOutputStream arrayOutputStream) {
        // TODO Auto-generated method stub
        try {
            if (arrayOutputStream != null) {
                arrayOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
