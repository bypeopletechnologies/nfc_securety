package com.bypt4.supervisornfc.guard;


import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.BaseFragment;
import com.bypt4.supervisornfc.NFCCallback.NfcCallback;
import com.bypt4.supervisornfc.R;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.Utils.LogUtils;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;

import java.util.Arrays;
import java.util.List;


public class GuardNameFragment extends BaseFragment implements NfcCallback {
    private static final String TAG = "GuardNameFragment" ;
    private View rootView;
    private TextView tvGuardLocationText,etGuardLocation;
    private ImageView ivLocationNext;
    public Tag tag;
    public Bundle sendData;
    String locationId,locationName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_guard_name, container, false);
            init();
            setListeners();
            setFontStyle();
            return rootView;
        }else {
            return rootView;
        }

    }

    public void init(){
        etGuardLocation = (TextView) rootView.findViewById(R.id.etGuardLocation);
        tvGuardLocationText = (TextView) rootView.findViewById(R.id.tvGuardLocationText);
        ivLocationNext = (ImageView) rootView.findViewById(R.id.ivLocationNext);

        sendData = new Bundle();

        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(SharedPreferenceUtil.
                getString(Constants.EMPLOYEE_NAME,getString(R.string.guard_name)));
        ((BaseActivity)mContext).showHideToolbarBackButton(false);
        ((BaseActivity)mContext).showHideToolbarRightButton(true);
        ((BaseActivity)mContext).setNFCResponseCallBack(this);

    }
    public void setListeners(){
        ivLocationNext.setOnClickListener(this);
    }

    public void setFontStyle(){
        setTypeFaceTextView(tvGuardLocationText,getContext().getResources().getString(R.string.fontLatoRegular));
        setTypeFaceTextView(etGuardLocation,getContext().getResources().getString(R.string.fontLatoLight));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseActivity)mContext).showHideToolbarBackButton(false);
        ((BaseActivity)mContext).showHideToolbarRightButton(true);
    }
    private boolean isValid(){
        if(etGuardLocation.getText().toString().trim().length() <= 0){
            showDialogAlertPositiveButton(getString(R.string.app_name),
                    getString(R.string.alert_security_location));
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.ivLocationNext:
                if(isValid()) {
                    Fragment fragment = new GuardSecurityStatusFragment();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    LogUtils.LOG_E(TAG, "LocationId" + locationId);
                    sendData.putString("LocationId", locationId);
                    fragment.setArguments(sendData);
                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer, fragment);
                    fragmentTransaction.addToBackStack("GuardNameFragment");
                    fragmentTransaction.commit();
                }
                break;

        }
    }

    @Override
    public void onResponse(String response, int action) {
        if(response != null){
            List<String> list = Arrays.asList(response.split(","));
            if(list.size() != 0 ){
                locationId = list.get(0);
                LogUtils.LOG_E(TAG,"LocationId"+locationId);
            }
            LogUtils.LOG_E(TAG,"LocationId"+list.get(1));
            etGuardLocation.setText(list.get(1));
        }

    }
}
