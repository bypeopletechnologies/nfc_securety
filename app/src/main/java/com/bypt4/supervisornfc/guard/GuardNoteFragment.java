package com.bypt4.supervisornfc.guard;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.BaseFragment;
import com.bypt4.supervisornfc.R;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.Utils.LogUtils;
import com.bypt4.supervisornfc.Utils.NetworkUtil;
import com.bypt4.supervisornfc.http.HttpCallback;
import com.bypt4.supervisornfc.http.HttpPostRequest;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GuardNoteFragment extends BaseFragment implements View.OnClickListener, HttpCallback {

    private static final String TAG = "GuardNoteFragment" ;
    private View rootView;
    private EditText etGuardNote;
    private ImageView ivGuardSave;
    private TextView tvNote;
    private Bundle getData;
    private String status,locationId,description;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_guard_note, container, false);
            init();
            setListeners();
            setFontStyle();
            return rootView;
        }else {
            return rootView;
        }

    }

    private void setFontStyle() {

            setTypeFaceTextView(tvNote,mContext.getString(R.string.fontLatoRegular));
            setTypeFaceEdittext(etGuardNote,mContext.getString(R.string.fontLatoRegular));
    }

    private void setListeners() {
        ivGuardSave.setOnClickListener(this);
    }

    public void init(){

        etGuardNote=(EditText)rootView.findViewById(R.id.etGuardNote);
        ivGuardSave=(ImageView)rootView.findViewById(R.id.ivGuardSave);
        tvNote=(TextView)rootView.findViewById(R.id.tvNote);

        getData = new Bundle();

        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle(SharedPreferenceUtil.getString
                (Constants.EMPLOYEE_NAME,getString(R.string.guard_name)));
        ((BaseActivity)mContext).showHideToolbarBackButton(true);
        ((BaseActivity)mContext).showHideToolbarRightButton(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getData = getArguments();
        if(getData != null){
             status = getData.getString("Status");
            locationId = getData.getString("LocationId");
            LogUtils.LOG_I(TAG,status);
            LogUtils.LOG_E(TAG,locationId);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivGuardSave:
                if(isValidNotes()) {
                    addLocation();
                }
                break;
        }
    }

    private boolean isValidNotes() {
        if (etGuardNote.getText().toString().trim().length() < 1) {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_enter_note));
            etGuardNote.requestFocus();
            return false;
        }
        return true;
    }



    private void addLocation() {
        if (NetworkUtil.isOnline(mContext)) {
            progressDialog.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("employee_id", SharedPreferenceUtil.getString(Constants.EMPLOYEE_ID, ""));
            params.put("sessionCode", SharedPreferenceUtil.getString(Constants.EMPLOYEE_SESSIONCODE, ""));
            params.put("location_id",locationId);
            params.put("description",etGuardNote.getText().toString());
            params.put("security_status",status);
            HttpPostRequest request = new HttpPostRequest(mContext.getString(R.string.base_url)
                    + "addLocation", params, Constants.REQUEST_FOR_ADDLOCATION, this);
            new Thread(request).start();
        } else {
            showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.please_check_network));
        }

    }

    @Override
    public void onResponse(final String response, int action) {
        progressDialog.dismiss();
        LogUtils.LOG_E("RES:",response);

        if (HttpPostRequest.statusCode != Constants.SERVER_ERROR) {
            if (response != null) {
                if (action == Constants.REQUEST_FOR_ADDLOCATION) {
                    LogUtils.LOG_E("Add Location Res:",response);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    //Handle Response Of AddLocation

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                    builder.setMessage("Security Details Added Successfully")
                                            .setCancelable(false)
                                            .setTitle(getString(R.string.app_name))
                                            .setPositiveButton(mContext.getString(R.string.ok),
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                            Fragment fragment = new GuardNameFragment();
                                                            fragmentTransaction = fragmentManager.beginTransaction();
                                                            fragmentTransaction.replace(R.id.frameLayoutBaseContainer,fragment);
                                                            fragmentTransaction.commitNowAllowingStateLoss();
                                                        }
                                                    });

                                    AlertDialog alert = builder.create();
                                    alert.show();


                                } else {
                                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), jsonObject.getString("message"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                    }
                });
            }
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    showDialogAlertPositiveButton(mContext.getString(R.string.alert_nfcsecurity), mContext.getString(R.string.server_not_reachable));
                }
            });
        }
    }


}


