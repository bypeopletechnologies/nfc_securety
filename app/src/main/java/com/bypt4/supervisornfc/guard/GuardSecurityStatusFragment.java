package com.bypt4.supervisornfc.guard;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ListPopupWindow;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.bypt4.supervisornfc.BaseActivity;
import com.bypt4.supervisornfc.BaseFragment;
import com.bypt4.supervisornfc.R;
import com.bypt4.supervisornfc.Utils.Constants;
import com.bypt4.supervisornfc.storage.SharedPreferenceUtil;

import java.util.ArrayList;

public class GuardSecurityStatusFragment extends BaseFragment {

    private View rootView;
    private TextView tvSecurityStatus;
    private EditText etSelectSecurityStatus;
    private ImageView ivSecurityStatusNext;
    private Bundle sendArgument,getArgument;
    private ListPopupWindow lpw;
    private String[] securityStatus = {"CLEAR","ALERT","FIRE","BREACH","MEDXNGY","POLICEXNGY"};
    private String locationId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_guard_security_status, container, false);
            init();
            setListeners();
            setFontStyle();
            return rootView;
        }else {
            return rootView;
        }
    }
    private void init(){

        tvSecurityStatus=(TextView)rootView.findViewById(R.id.tvSecurityStatus);
        etSelectSecurityStatus=(EditText)rootView.findViewById(R.id.etSelectSecurityStatus);
        ivSecurityStatusNext = (ImageView) rootView.findViewById(R.id.ivSecurityStatusNext);

        sendArgument = new Bundle();
        getArgument = new Bundle();

        ((BaseActivity)mContext).showToolbar(true);
        ((BaseActivity)mContext).setToolbarTitle((SharedPreferenceUtil.getString
                (Constants.EMPLOYEE_NAME,getString(R.string.guard_name))));
        ((BaseActivity)mContext).showHideToolbarBackButton(true);
        ((BaseActivity)mContext).showHideToolbarRightButton(false);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_list_item_1,securityStatus);
        lpw = new ListPopupWindow(mContext);
        lpw.setAdapter(dataAdapter);
        lpw.setAnchorView(etSelectSecurityStatus);
        lpw.setModal(true);

    }

    private void setFontStyle() {
        setTypeFaceTextView(tvSecurityStatus,mContext.getString(R.string.fontLatoRegular));
        setTypeFaceEdittext(etSelectSecurityStatus,mContext.getString(R.string.fontLatoLight));
    }

    private void setListeners() {
        etSelectSecurityStatus.setOnClickListener(this);
        ivSecurityStatusNext.setOnClickListener(this);
        lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String item = securityStatus[position];
                etSelectSecurityStatus.setText(item);
                lpw.dismiss();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getArgument = getArguments();
        if(getArgument != null){
            locationId = getArgument.getString("LocationId");
        }
    }


    private void showStatusList() {
        lpw.show();
    }

    private boolean isStatusSelected(){
        if(etSelectSecurityStatus.getText().toString().trim().length() <= 0){
            showDialogAlertPositiveButton(getString(R.string.app_name),
                    getString(R.string.alert_select_security_status));
            return false;
        }
        return true;

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.etSelectSecurityStatus:
                showStatusList();
                break;
            case R.id.ivSecurityStatusNext:
                if(isStatusSelected()){
                    Fragment fragment = new GuardNoteFragment();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    sendArgument.putString("Status",etSelectSecurityStatus.getText().toString());
                    sendArgument.putString("LocationId",locationId);
                    fragment.setArguments(sendArgument);
                    fragmentTransaction.replace(R.id.frameLayoutBaseContainer,fragment);
                    fragmentTransaction.addToBackStack("GuardSecurityStatusFragment");
                    fragmentTransaction.commit();
                }
                break;
        }
    }
}


